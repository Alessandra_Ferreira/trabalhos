﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.D_B_D
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script =
            @"INSERT INTO tb_produto
            (
                nm_produto, 
                vl_preco
            )
            VALUES
            (
                @nm_produto, 
                @vl_preco
            )";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            //E então???
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
  
        public List<ProdutoDTO> Listar()
        {
            string script =
                @"Select * from tb_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.ID = reader.GetInt32("id_produto");
                dto.Produto = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                lista.Add(dto);
            }
            reader.Close();
            return lista;

            //Dá uma olhada no DTO e faça uma comparação com os tipos de dados
        }

        public List<ProdutoDTO> Consultar(ProdutoDTO produto)
        {
            string script = @"SELECT * FROM tb_produto
                                    WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_produto", "%" + produto.Produto + "%"));

            //Eu sei que a senhorita já sabe como fazer esse método, mas como a senhorita pôde ter visto na área de paramêtros...
            //Existem uma variável que carrega um DTO, com essa variável iremos pegar as informações do cliente, e a partir desta informação...
            //Utilizar o comando LIKE que, por sua vez, faz a procura de palavras-chaves de modo inexato.

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            while (reader.Read())
            {
              ProdutoDTO dto = new ProdutoDTO();
              dto.ID = reader.GetInt32("id_produto");
              dto.Produto = reader.GetString("nm_produto");
              dto.Preco = reader.GetDecimal("vl_preco");
              lista.Add(dto);
            }
            reader.Close();
            return lista;

        }
    }
}
