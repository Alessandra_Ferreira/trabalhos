﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.D_B_D
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            //Esse tipo de validação é bem simples!!!
            //Ele pega o valor do DTO, com isso, ele mapeia o campo correto, e após isso, faz uma condição de fácil compreensão.
            if (dto.Produto == string.Empty)
            {
              throw new ArgumentException("O nome do produto PRECISA ser preenchido");
            }
            if (dto.Preco == 0)
            {
              throw new ArgumentException("O valor PRECISA ser descrito");
            }

            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            List<ProdutoDTO> list = db.Listar();

            return list;
        }

        public List<ProdutoDTO> Consultar(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            List<ProdutoDTO> list = db.Consultar(dto);

            return list;
        }
    }
}
