﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.D_B_D
{
    class ProdutoDTO
    {
            public int ID { get; set; }
            public string Produto { get; set; }
            public decimal Preco { get; set; }
            
            //Como a senhorita pôde notar, existem somente três campos na tabela de Produtos...
            //Sabendo que a classe DTO mapeia os campos de uma tabela em banco de dados, logo existem 3 tabelas na classe dto
    }
}
