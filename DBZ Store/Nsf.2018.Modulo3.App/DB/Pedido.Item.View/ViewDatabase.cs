﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido.Item.View
{
    class ViewDatabase
    {
        public List<ViewDTO> Consultar (string cliente)
        {
            string script =
            @"SELECT * FROM vw_pedido_consultar
                WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_cliente", "%" + cliente + "%"));

            //Assim como na tb_produto, aqui utilizamos o método CONSULTAR, mas diferentemente do anterior...
            //Eu utilizei apenas uma variável com um tipo de dado (String) mais comum.
            
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            List<ViewDTO> fora = new List<ViewDTO>();
            while (reader.Read())
            {
                ViewDTO dentro = new ViewDTO();
                dentro.IDPedido = reader.GetInt32("id_pedido");
                dentro.Cliente = reader.GetString("nm_cliente");
                dentro.Data = reader.GetDateTime("dt_venda");
                dentro.IDPedidoItem = reader.GetInt32("qtd_itens");
                dentro.Preco = reader.GetDecimal("vl_total");

                fora.Add(dentro);
                //Piada ruim???
            }
            reader.Close();
            return fora;
        }
    }
}
