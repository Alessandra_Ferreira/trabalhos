﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido.Item.View
{
    class ViewDTO
    {
        public int IDPedido { get; set; }
        public string Cliente { get; set; }
        public DateTime Data { get; set; }
        public int IDPedidoItem { get; set; }
        public decimal Preco { get; set; }

        //Quando você for fazer essa parte, olhe bem o SCRIPT do professor.
        //Você pode notar que após o comando SELECT, são alocados vários campos, e a partir desses campos, você irá mapea-lo com a classe DTO.
    }
}
