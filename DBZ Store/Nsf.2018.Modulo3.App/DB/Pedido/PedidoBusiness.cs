﻿using Nsf._2018.Modulo3.App.D_B_D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        /*Aqui será realizado a escrita do codigo de salvamento dos produtos , ele 
         irá ligar o dto e o database de modo simples , pois pegará os dados que a tela adicionou no dto,
         passará os dados aqui e enviará para o database que executará o script que enviará para o MySql*/

        public int Salvar(PedidoDTO pedido , List<ProdutoDTO> produtos) 
        {
            PedidoDatabase pedidodatabase = new PedidoDatabase();
            int idPedido = pedidodatabase.Salvar(pedido);

            /* foreach irá percorrer o dto e salvar na lista e depois irá aparecer na tela */
            Business_PedidoItem itembusiness = new Business_PedidoItem();
                        
            foreach (ProdutoDTO item in produtos)
            {
                DTO_PedidoItem itemdto = new DTO_PedidoItem();
                itemdto.IDPedido = idPedido;
                itemdto.IDProduto = item.ID ;
                /*salva tudo que foi salvo no dto do pedidoitem*/
                itembusiness.Salvar(itemdto);
              
            }
            return idPedido;

            //Nessa parte, creio que a senhorita tenha se perdido um pouco no quesito LÓGICA.
            //Como por exemplo: Na área de paramêtros. Você colocou 2 DTOs para o pedido, quando na verdade, era um DTO para o pedido e o outro para o Produto...
            //Reveja os seus erros "E TENTE NOVAMENTE!!"        
        }
        public void Remover (int id)
        {
            PedidoDatabase db = new PedidoDatabase();
            db.Remover(id);
        }
    }
}
