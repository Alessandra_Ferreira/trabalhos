﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.D_B_D;
using Nsf._2018.Modulo3.App.DB.Pedido;
using System.Text.RegularExpressions;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        public frmPedidoCadastrar()
        {
            InitializeComponent();
            /*Foram criadas duas funções para que se tornasse possivel */
            CarregarCombos(); /* Carrega as variaveis do dto do produto */
            ConfigurarGrid();/*Configurações da grid da tela*/
            
        }

        void CarregarCombos()
        {
            /**/
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Produto);
            cboProduto.DataSource = lista;

        }
        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            string re = @"^\d\d\d[.]\d\d\d[.]\d\d\d[-]\d\d$";

            
            PedidoDTO dto = new PedidoDTO();
            dto.Cliente = txtCliente.Text;
            dto.Cpf = txtCpf.Text;
            dto.Data = DateTime.Now;

            Regex regex = new Regex(re);

            if (regex.IsMatch(dto.Cpf) == false)
            {
                MessageBox.Show("Test");

                //ao inves de usar o else pode ultilizar o return.
            }
            else
            {
                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto, produtosCarrinho.ToList());
                MessageBox.Show("Pedido Salvo com Sucesso",
                                "Pedido!",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);

                //Nessa tela, estava tudo "LINDO E MARAVILHOSO", todavia, por conta do seu erro quando estava mapeando a tabela, acarretou em nesse erro...
                //Mas nada para se preocupar, creio que seja por falta de treino!!!
                this.Hide();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

           
            for (int i = 0; i < int.Parse(txtQuantidade.Text); i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 1)
            {
                PedidoBusiness dbs = new PedidoBusiness();
                PedidoDTO dto = dgvItens.CurrentRow.DataBoundItem as PedidoDTO;

                DialogResult r = MessageBox.Show("Deseja excluir a música?", "Nsf Jam",
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    PedidoBusiness business = new PedidoBusiness();
                    business.Remover(dto.ID);
                    
                }          

            }
        }
        public void Stars()
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;


                       
        }
    }
}
