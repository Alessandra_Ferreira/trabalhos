﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Pedido.Item.View;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoConsultar : UserControl
    {
        public frmPedidoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cliente = txtCliente.Text;

            ViewDatabase db = new ViewDatabase();
            List<ViewDTO> list = db.Consultar(cliente);

            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = list;

            //Essa é uma maneira mais simplificada de se fazer o método de consultar. 
            //Neste caso, estou utilizando uma variável de caractere para obter o valor do controle
            //Mas assim como as outras classes já aprendidas, podemos criar uma classe que sirva de paramêtro (DTO)

            //this.Hide();
        }
    }
}
