﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.D_B_D;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = new ProdutoDTO();
            dto.Produto = txtProduto.Text;
            dto.Preco = Convert.ToDecimal(txtPreco.Text);

            ProdutoBusiness business = new ProdutoBusiness();
            business.Salvar(dto);
            MessageBox.Show("Produto Cadrastado com sucesso", 
                            "Cadastramento de Novo Produto", 
                            MessageBoxButtons.OK, 
                            MessageBoxIcon.Information);
            
            //Olha... EU creio que a senhorita seja mais que apta para compreender todo fluxo que esta acontecendo nessa parte do código...
            //Mas a senhorita demonstrou alguns erros bem repentino, todavia, porém, eu creio que tenha sido pela sua exausta e puxada rotina semanal.


            this.Hide();
        }
    }
}
