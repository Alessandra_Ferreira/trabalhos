﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.D_B_D;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoConsultar : UserControl
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = new ProdutoDTO();
            dto.Produto = txtProduto.Text;

            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Consultar(dto);

            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = lista;
          
            //Tenho que ser honesto, em todas as telas a senhorita pecou exatamente nesse momento.
            //Você esqueceu tanto de atribuir um valor a lista, quanto de deixar com que a geração automáticas de colunas fiquem falsas (Em relação a DataGridView)

        }
    }
}
